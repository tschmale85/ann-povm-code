import unittest
import numpy as np
import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], '..'))

import source.quantum as qu
from source.quantum import dms, povms
from source.util import isClose


class TestDMs(unittest.TestCase):
    def setUp(self) -> None:
        print("Make sure to run these tests with 64-bit float precision, otherwise tests might fail!")
        np.random.seed(1)

        self.N = 6

    def testPurePurity(self):
        state = np.random.normal(size=2 ** self.N) + 1j * np.random.normal(size=2 ** self.N) #generate random pure state
        state /= np.linalg.norm(state)

        # compute subsystem purity of first 2 particles, under the presence of dephasing noise
        subpurity1 = qu.subsystem_purity(state, dephasing=0.1, num=2)

        rho = 0.9 * qu.pureStateToDM(state) + 0.1 * np.eye(2 ** self.N) / 2 ** self.N #compute corresponding d.m.

        rho_t = dms.opToTensorOp(rho)
        rho_2 = dms.partial_trace_t(rho_t, [2, 3, 4, 5]) # trace out desired subsystem

        subpurity2 = dms.purity(dms.tensorOpToOp(rho_2)) # compute purity of said subsystem
        self.assertTrue(isClose(subpurity1, subpurity2)) # cross check results

    def testMLE(self):
        N = 3
        state = np.linalg.eigh(dms.getRandomRho(2**N))[1][:, 0]
        rho = qu.pureStateToDM(state)
        povm = povms.POVM()
        samples = povm.getSamplesFromPOVMDist(povm.getPOVMDistribution(rho), 10**6)

        rho_mle = dms.subsystem_mle(np.array(samples), [0, 1, 2], povm)

        self.assertTrue(0.95 < dms.fidelity(rho, rho_mle) < 1)

        rho_2 = dms.tensorOpToOp(dms.partial_trace_t(dms.opToTensorOp(rho), [2]))
        rho_2_mle = dms.subsystem_mle(samples, [0, 1], povm)

        print(dms.fidelity(rho_2, rho_2_mle))
        self.assertTrue(0.95 < dms.fidelity(rho_2, rho_2_mle) < 1)


if __name__ == '__main__':
    unittest.main()
