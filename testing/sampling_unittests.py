import unittest
import numpy as np

import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], '..'))

import source.quantum as qu
from source.quantum import ising, povms, dms
from source import sampling, util
import jax

class MyTestCase(unittest.TestCase):
    def test_local_sampling(self):
        N = 4
        state = np.linalg.eigh(dms.getRandomRho(2**N))[1][:, 0]#ising.pureGroundState(1, 1, N) # Generate target state
        povm = povms.POVM(povms.pauli4POVM())

        samples = sampling.getLocalSamples(jax.random.PRNGKey(1), povm.ppp(state, batch_size=1000, do_jit=False),
                                           N, num_samples=10**6, num_chains=100) #Generate samples
        samples = np.array(samples)
        _, dataset_dist = util.getDatasetWeights(samples, return_dist=True) #Compute frequency distribution
        dataset_dist = povms.tensorDistTo1D(dataset_dist)

        target = povm.getPOVMDistribution(qu.pureStateToDM(state)) # Compute target distribution
        print(1-util.fidelity(dataset_dist, target))
        self.assertTrue(1 - util.fidelity(dataset_dist, target) < 10**-4) # Check if both distributions are close

        dist = np.array(povm.getPOVMDistribution(qu.pureStateToDM(state)))
        samples2 = povm.getSamplesFromPOVMDist(dist/np.sum(dist), 10**6)
        samples2 = np.array(samples2)
        _, dataset_dist2 = util.getDatasetWeights(samples2, return_dist=True)  # Compute frequency distribution
        dataset_dist2 = povms.tensorDistTo1D(dataset_dist2)

        print(1 - util.fidelity(dataset_dist2, target))
        self.assertTrue(1 - util.fidelity(dataset_dist2, target) < 10 ** -4)  # Check if both distributions are close


if __name__ == '__main__':
    unittest.main()
