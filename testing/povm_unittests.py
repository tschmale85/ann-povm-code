import unittest
import numpy as np
import qutip

import sys
import os

sys.path.insert(1, os.path.join(sys.path[0], '..'))

import source.quantum as qu
from source.quantum import dms, povms
from source.util import isClose
import source
import itertools

class TestPovmFormalism(unittest.TestCase): #Todo: run multiple of these tests
    def setUp(self) -> None:
        print("Make sure to run these tests with 64-bit float precision, otherwise tests might fail!")
        np.random.seed(1)

        self.N = 6
        self.rho = np.array(qutip.rand_dm(2**self.N))

        a = np.random.choice(range(self.N - 1))
        b = np.random.choice(range(a + 1, self.N))

        if np.random.random() < 0.5:
            a, b = b, a
        self.targets = [a, b]

        self.povm = povms.default_povm

        # generate random unitary
        self.U_base = qutip.rand_unitary(4, dims=[[2, 2], [2, 2]])
        self.U = np.array(self.U_base)

    def testShapeConversion(self):
        self.assertTrue(isClose(self.rho, dms.tensorOpToOp(dms.opToTensorOp(self.rho))))

    def testDistributionGenerationAndInversion(self):
        dist = self.povm.getPOVMDistribution(self.rho)

        self.assertTrue(isClose(np.sum(dist), 1)) #Check dist sums to 1
        self.assertTrue(np.min(dist) > -source.errorMargin) #Check dist is positive => it's a probability distribution

        rho_ = self.povm.linRecon(dist, self.N)
        self.assertTrue(isClose(self.rho, rho_))

    def testMatrixConventions(self):
        # Not really a test. This just computes UU^* == 1 in the tensor formalism and in the matrix formalism as d demo.
        temp_t = dms.opToTensorOp(self.U)
        self.assertTrue(isClose(dms.tensorOpToOp(np.einsum('abcd, dcfe->abef', temp_t, temp_t.T.conj())), np.eye(4)))
        self.assertTrue(isClose(self.U @ self.U.T.conj(), np.eye(4)))
        # Same for computing a trace
        self.assertTrue(isClose(np.trace(self.U), np.einsum('ijij', temp_t)))

    def testApplyUnitary(self):
        #Compute U*rho*U^* in povm formalism
        dist_t = self.povm.getPOVMDistribution(self.rho, return_1D=False)
        U_povm = self.povm.getPovmUnitary_2ptcl(dms.opToTensorOp(self.U))
        self.assertTrue(isClose(np.sum(self.povm.tensorPovmUnitaryToMatrix(U_povm), axis=0), np.ones(4**2)))

        transformed_dist_povms = self.povm.tensorDistTo1D(self.povm.applyUnitary(dist_t, U_povm, self.targets))
        transformed_rho_povms = self.povm.linRecon(transformed_dist_povms, self.N)

        #Compute U*rho*U^* using qutip as a reference
        U_qutip = qutip.circuit.gate_expand_2toN(self.U_base, self.N, targets=self.targets)
        transformed_rho_qutip = U_qutip * self.rho * U_qutip.dag()
        transformed_dist_qutip = self.povm.getPOVMDistribution(transformed_rho_qutip)

        self.assertTrue(isClose(transformed_dist_qutip, transformed_dist_povms)) #check equality of distributions
        self.assertTrue(isClose(transformed_rho_qutip, transformed_rho_povms)) #check equality of reconstructed d.m.'s

    def testExpVal(self): #Todo: test operators acting on multiple particles (not just 2)
        H_base = qutip.rand_herm(4, dims=[[2, 2], [2, 2]])
        H = np.array(H_base)

        #Compute Tr(H\rho) in povm formalism
        dist_t = self.povm.getPOVMDistribution(self.rho, return_1D=False)
        H_povm = self.povm.getPovmObservable(dms.opToTensorOp(H))

        exp_val_povms = self.povm.expVal(dist_t, H_povm, [int(x) for x in self.targets])

        #Compute Tr(H\rho) using qutip as a reference
        H_qutip = qutip.circuit.gate_expand_2toN(H_base, self.N, targets=[int(self.N-1-x) for x in self.targets[::-1]])
        exp_val_qutip = dms.expVal(H_qutip, self.rho)

        self.assertTrue(isClose(exp_val_povms, exp_val_qutip)) #check equality of distributions

    def testProbabilityCalculation(self):
        state = np.random.normal(size=2 ** self.N) + 1j * np.random.normal(size=2 ** self.N)
        state /= np.linalg.norm(state)

        dist_t = self.povm.getPOVMDistribution(qu.pureStateToDM(state), return_1D=False)

        s = 0
        max_diff = 0
        for a in itertools.product(range(4), repeat=self.N):
            s += np.exp(self.povm.logP(a, state))
            diff = np.abs(np.exp(self.povm.logP(a, state)) - dist_t[tuple(a)])
            if diff > max_diff:
                max_diff = diff

        self.assertTrue(isClose(max_diff, 0))
        self.assertTrue(isClose(s, 1))

    def testPurityAndPartialTrace(self):
        rho = self.rho
        povm = self.povm

        purity = dms.purity(rho) #Compute purity of matrix
        dist_t = povm.dist1DToTensor(povm.getPOVMDistribution(rho), self.N)

        purity2 = povm.purity(dist_t, self.N) #compute purity of distribution
        self.assertTrue(isClose(purity, purity2)) #cross check both

        rho_t = dms.opToTensorOp(rho)
        rho_2 = dms.partial_trace_t(rho_t.T, [2, 3, 4, 5]) # trace out subsystem on matrix
        self.assertTrue(dms.isPhysical(dms.tensorOpToOp(rho_2))) # check if result is still a physical d.m.

        subpurity = dms.purity(dms.tensorOpToOp(rho_2)) # compute purity of said subsystem on matrix

        dist_2 = povm.partialTrace(dist_t, [0, 1]) # trace out same subsystem on distribution

        self.assertTrue(isClose(np.sum(dist_2), 1)) # check distribution still sums to 1

        subpurity2 = povm.purity(dist_2) # compute subsystem purity on distribution
        self.assertTrue(isClose(subpurity, subpurity2)) # cross check both results

        samples = povm.getSamplesFromPOVMDist(povm.tensorDistTo1D(dist_t), 1000000) # get samples from full distribution
        samples = np.array(samples)[:, :2] # 'trace out' same subsystem on samples
        sampled_subpurity = povm.sample_purity(samples) # compute purity from samples and cross check
        self.assertTrue(np.abs(subpurity - sampled_subpurity[0] < sampled_subpurity[1])) # Check if error < 1 std


if __name__ == '__main__':
    unittest.main()
