import unittest
from source.util import isClose

from source.quantum import graph_states, povms
from source import quantum as qu
import qutip.qip.circuit as qcirc
import qutip
import numpy as np
import source
from parameterized import parameterized


class TestGraphStates(unittest.TestCase):

    @parameterized.expand([(x, ) for x in range(2, 11)])
    def testGraphStates(self, N):
        # Generate random graph
        np.random.seed(42) #if reproducibility needed

        nbonds = np.random.choice(range(0, int(N*(N-1)/2)))
        print(f'Testing {N} particles, {nbonds} bonds.')

        graph = graph_states.generateRandomGraph(N, nbonds=nbonds)
        #print(f'Graph: {graph}')

        #Compute POVM distribution in POVM formalism
        dist_povm = graph_states.generatePOVMDistForGraph(N, graph)

        self.assertTrue(isClose(np.sum(dist_povm), 1))  # Check dist sums to 1
        self.assertTrue(np.min(dist_povm) > -source.errorMargin)  # Check >= 0 => it's a probability distribution

        #Compute POVM distribution using qutip
        q = qcirc.QubitCircuit(N, reverse_states=False)
        q.add_1q_gate('SNOT', qubits=list(range(N)))
        for pair in graph:
            q.add_gate('CSIGN', controls=[int(pair[0])], targets=[int(pair[1])])

        U = qcirc.gate_sequence_product(q.propagators())
        initial_state = qutip.qubits.qubit_states(N, [0] * N)
        final_state = U * initial_state

        rho_qutip = qu.pureStateToDM(np.array(final_state))
        dist_qutip = povms.getPauli4Dist(rho_qutip, return_1D=False)

        self.assertTrue(isClose(dist_povm, dist_qutip))


if __name__ == '__main__':
    unittest.main()
