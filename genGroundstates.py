import argparse
import numpy as np

from source.quantum import ising

parser = argparse.ArgumentParser()
parser.add_argument("J", help="Coupling const.", type=float)
parser.add_argument("B", help="External Field", type=float)
parser.add_argument("N", help="Particle No.", type=int)
parser.add_argument("--dim", help="Dimension (1 or 2)", type=int, default=1)
parser.add_argument("--alpha", help="Decay of interaction", type=float, default=None)
args = parser.parse_args()

state = ising.pureGroundState(args.J, args.B, args.N, dim=args.dim, alpha=args.alpha)

config = {
    "N": args.N,
    "J": args.J,
    "B": args.B,
    "dim": args.dim,
    "alpha": args.alpha
}

name = str(config).replace(":", "=").replace(" ", "") + ".npy"

np.save("data/states/" + name, state)
