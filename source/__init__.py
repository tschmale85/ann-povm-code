import json
import logging.handlers
from pathlib import Path
import time

import numpy as np
import pickle
import progressbar as pb

dtypeC = np.complex128
dtypeR = np.float64

errorMargin = 1e-7

import source.quantum as qu
from source.quantum import povms, ising, graph_states, dms, cnot_states
from source.ann import trainingSampled, Logging
from source import util, sampling

logging.getLogger().setLevel(logging.INFO)
logger_ = logging.getLogger()
logger_.handlers = []
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
ch.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(message)s'))
logger_.addHandler(ch)

def info(msg):
    logger_.info(msg)

def warn(msg):
    logger_.warning(msg)


def learnWithAverage(params, state, doPrint=False, saveDir=None):
    repeat = params.get('repeat', 10)

    args = dict(params)
    args['actFun'] = args['actFun'].__name__

    #Build file name
    name_dict = {}
    for name_key in params.get('name_keys', []):
        if name_key in args:
            name_dict[name_key] = args[name_key]
    name = str(name_dict).replace(" ", "").replace(":", "=")
    # json would be nicer but cant have : in windows file name

    N = params['N']

    info(name)
    info(str(args))

    assert saveDir is not None
    saveDir = './' + saveDir + '/'
    Path(saveDir).mkdir(parents=True, exist_ok=True)
    file_name = name + ", " + str(time.time())

    #Which datapoints to collect manually
    collect_cols = ['numSteps', 'duration']  # 'finalKL'
    data = dict()
    for key in collect_cols:
        data[key] = []

    #Determine number of training samples
    num_samples = -1
    if params.get('s', -1) != -1:
        num_samples = int(params.get('s')*4**N)
    elif params.get('Ns', -1) != -1:
        num_samples = int(params.get('Ns'))

    #Get samples and target
    samples, target, state_vectors = getSamples(state, params, num_samples, N)

    #Setup automatic data collection on performance and physical quantities
    distribution_loggers, physical_loggers = getLoggers(N, target, dephasing=params['u'])
    all_loggers = list(distribution_loggers.values()) + physical_loggers

    #Extract physical quantities from bare training samples as a reference
    if not callable(target):
        for logger in physical_loggers:
            logger.log('Truth', distribution=povms.dist1DToTensor(target, N))

    result_ = None
    #Train NN on training samples several times
    for i in pb.progressbar(range(repeat)):
        result_ = trainingSampled.learn(params, samples, target, distribution_loggers, physical_loggers,
                                        doPrint=doPrint)

        with open(f"{saveDir}{file_name}{i}_samples.txt", "w") as file:
            for sample in result_["final_nn_samples"]:
                file.write(str(sample)[1:-1].replace('\n', '').replace(' ', '') + "\n")

        for key in collect_cols: #Manually collect some data
            if key in result_:
                data[key].append(result_[key])

        # For small systems, compare to iMLE
        if N < 8:
            info(f"Comparing to iMLE")
            dataset_dist = povms.tensorDistTo1D(util.getDatasetWeights(samples, return_dist=True)[1])
            rho_imle = dms.iMLE(dataset_dist, povms.default_povm, N)
            distribution_iMLE = povms.getPauli4Dist(rho_imle)

            if callable(target):
                basis = ann.getBasis(N)
                target = target(basis)
            imle_dist = 1 - util.fidelity(distribution_iMLE, target)
            imle_dataset_dist = 1 - util.fidelity(distribution_iMLE, dataset_dist)
            distribution_loggers['true_fid'].add_value('iMLE', imle_dist)
            distribution_loggers['dataset_fid'].add_value('iMLE', imle_dataset_dist)

            for logger in physical_loggers: #Compute observables for MLE
                logger.log('iMLE', distribution=povms.dist1DToTensor(distribution_iMLE, N))

        info("Comparing to Dataset and local MLE")
        for logger in physical_loggers: #Compute observables from samples and perform local MLE
            logger.log("Dataset", samples=samples)
            #logger.log("Local MLE", samples_for_mle=samples)

        #half_samples = np.array(samples)[:, :N//2]
        #rho_half = dms.fast_iMLE(half_samples, povms.default_povm)
        #physical_loggers[2].add_value('Samples iMLE', dms.purity(rho_half))

        for logger in all_loggers:
            logger.newSeries()

        info(f"Done! ({i + 1}/{repeat})" + name)

        if params.get('avgDatasets', True) and i != repeat-1: #Generate new samples for each repetition (except final)
            info('Getting new samples!')
            samples, *_ = getSamples(state, params, num_samples, N)

    info("Computing exact observables!")
    if state_vectors is not None: #Compute exact observables
        for logger in physical_loggers:
            logger.log("Truth", pure_states=state_vectors)

    data['numParams'] = int(result_['numParams'])
    data['config'] = args
    info("Saving results!")

    pickle.dump(all_loggers, open(saveDir + "/" + file_name + ".pkl", "wb")) #Save results as pickle
    Logging.plotLoggers(all_loggers, saveDir + "/" + file_name + ".pdf") #Plot run

    for logger in all_loggers:  # Extract data from loggers that is to be saved in plain text
        data[logger.name] = logger.toDict()

    with open(f"{saveDir}{file_name}.txt", "w") as myfile: #Save results as text file
        myfile.write(json.dumps(data, indent=2))

    return data

def getTarget(params, state: str): #Compute exact target POVM distributions for some states
    N = params['N']
    target = None

    if state == 'ising':

        info("Computing Ground state")
        rho = ising.computeGroundState(params['J'], params['B'], N, u=params.get('u', 0), dim=params['dim'],
                                       alpha=params['alpha'])
        info("Computing target distribution")
        target = povms.getPauli4Dist(rho, N)
        info("Done computing target")

    elif state == 'graph':
        doCluster = params.get('cluster', False)
        doPeriodicCluster = params.get('pCluster', False)
        #if doCluster or doPeriodicCluster:
        #    graph = [[i, i + 1] for i in range(N - 1)]
        #    if doPeriodicCluster:
        #        graph.append([N - 1, 0])
        #else:
        #    graph = graph_states.generateRandomGraph(N, nbonds=N)
        #target = povms.tensorDistTo1D(graph_states.generatePOVMDistForGraph(N, graph))
        target, _ = graph_states.get_experimental_state_and_samples(1000)
    elif state == 'ghz':
        rho_ghz = qu.GHZ(N)
        target = povms.getPauli4Dist(rho_ghz)

    target = util.safeAbs(target)
    return target

def getSamples(state, params, num_samples, N):
    state_vectors = None
    # Get samples from desired target distribution
    if state == 'cluster_mps':
        target = graph_states.ClusterMPS(N).P
        samples = graph_states.ClusterMPS.loadSamples(f'data{N}.txt', num_samples)
    elif state == 'exp':
        target, samples = graph_states.get_experimental_state_and_samples(num_samples)
    elif state == 'cnot':
        #samples, target, sampled, state_vector
        samples = sampling.loadSamples(f'cnot{N}.txt', num_samples, dephasing=params['u'])
        state_vectors = cnot_states.get_state(N)
        target = povms.ppp(state_vectors, dephasing=params['u'])

    elif params['liveSamples'] == 1: #Live samples -> compute samples now and don't read from file
        info("Computing exact target!")
        target = getTarget(params, state)
        samples = povms.getSamplesFromPOVMDist(target, num_samples)
    elif state == 'ensemble':
        samples = sampling.loadSamples(params['samples'], num_samples, absolute_path=True)

        with open(params['states'], 'rb') as states_file:
            states = pickle.load(states_file)
        state_vectors = np.array([state.full() for state in states]).squeeze() # shape = (num states, 2**N)

        target = lambda a: np.nan # Return callable dummy target...

    else:
        info("Only comparing to samples!")
        if state == 'ising':
            state_vectors = ising.loadState(params['J'], params['B'], params['N'],
                                            dim=params['dim'], alpha=params['alpha'])

            target = povms.ppp(state_vectors, dephasing=params['u'])
            samples = ising.loadSamples(params['J'], params['B'], params['N'], num=num_samples,
                                        dim=params['dim'], dephasing=params['u'], alpha=params['alpha'])

        else:
            warn('Unknown state...')
            return

    if state_vectors is not None and state_vectors.ndim == 1:
        # Reshape pure states to (1, 2**N) so they can be treated as ensembles
        state_vectors = np.expand_dims(state_vectors, axis=0)
    return samples, target, state_vectors

def getLoggers(N, target, dephasing=0):
    distribution_loggers = {
        'loss': Logging.Logger('Loss'),
        'grad': Logging.Logger('|Gradient|'),
        'true_fid': Logging.Logger('True Fidelity', log_scale=True),
        'dataset_fid': Logging.Logger('Dataset Fidelity', log_scale=True)
    }
    sz2_op = qu.opsListToBigOp([qu.sz, qu.sz])
    sx_op = qu.sx
    sy3_op = qu.opsListToBigOp([qu.sz, qu.sz, qu.sz])
    physical_loggers = [
        #Logging.ObservableLogger('Sx', sx_op, None, dephasing=dephasing),
        Logging.ObservableLogger('Sz', qu.sz, None, dephasing=dephasing),
        Logging.ObservableLogger('SzSzSz', sy3_op, [0, 1, 2], dephasing=dephasing),
        Logging.PurityLogger(num_sites=min(N//2, 7), dephasing=dephasing),
        Logging.PurityLogger(num_sites=1, dephasing=dephasing)
    ]
    physical_loggers += [Logging.ObservableLogger(f'Sz(0)Sz({i+1})', sz2_op, [0, i+1], dephasing=dephasing)
                         for i in range(N//2)]

    if N >= 6:
        physical_loggers.append(Logging.ObservableLogger('XZZ', qu.opsListToBigOp([qu.sx, qu.sz, qu.sz]),
                                                         [2, 3, 4], dephasing=dephasing))

        physical_loggers.append(Logging.ObservableLogger('XXZZ', qu.opsListToBigOp([qu.sx, qu.sx, qu.sz, qu.sz]),
                                                         [2, 3, 4, 5], dephasing=dephasing))
        physical_loggers.append(Logging.ObservableLogger('XXXZZ', qu.opsListToBigOp([qu.sx, qu.sx, qu.sx, qu.sz, qu.sz]),
                                                         [0, 1, 2, 3, 4], dephasing=dephasing))

    upup = qu.pureStateToDM(qu.up)
    physical_loggers += [Logging.ObservableLogger(f'1^{n}-Corr', qu.opsListToBigOp([upup]*n), list(range(n)),
                                                  dephasing=dephasing) for n in range(1, min(N//2, 7))]
    physical_loggers += [Logging.EntanglementEntropyLogger(n, dephasing=dephasing) for n in range(1, min(N//2, 7))]

    if not callable(target):
        physical_loggers += [Logging.NegativityLogger(), Logging.QuantumFidelityLogger(target)] #,Logging.Purity()]
    return distribution_loggers, physical_loggers
