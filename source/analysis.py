import os
import json

import glob

import joblib
import progressbar as pb
import matplotlib.pyplot as plt_
import numpy as np

def getRunDictsFromFolder(folder, max=None, new=False, onlyDone=False):

    #onlyfiles = [f for f in os.listdir(folder) if os.path.isfile(os.path.join(folder, f)) and ".txt" in f]
    onlyfiles = []
    for f in os.listdir(folder):
        if os.path.isfile(os.path.join(folder, f)) and ".txt" in f and (onlyDone is False or 'done' in f):
            onlyfiles.append(f)
        if max is not None and len(onlyfiles) >= max:
            break

    runs = []

    for f in pb.progressbar(onlyfiles):
        json_name = f.replace("True", "true").replace("\'", "\"").replace("=", ":")
        json_name = json_name[:json_name.index('}')+1]
        # print(f)
        param_dict = dict()
        try:
            param_dict = json.loads(json_name)
        except:
            print('File name not JSON...')
            print(f)
        # print(param_dict)
        with open(os.path.join(folder, f), "r") as file:
            if new:
                contents = ''.join(file.readlines())
            else:
                contents = file.readlines()[0]
            contents = contents.replace("nan", "-1").replace("-inf", "-1").replace("inf", "-1")
            contents = contents.replace("\'", "\"")
            # print(contents)
            try:
                contents = json.loads(contents)
                param_dict['results'] = contents
                runs.append(param_dict)
            except Exception as e:
                print('Error loading file contents: ', e)
                print(f, contents)

        # print(contents)
    return runs

def paramsMatch(targetParams, params):
    for key in targetParams:
        if key in params and targetParams[key] == params[key]:
            pass
        else:
            return False
    return True

def loadSamplesFromFile(file_name): # Equivalent to np.genfromtxt(file_name, delimiter=1), but 3x faster
    samples = []
    with open(file_name) as file:
        lines = file.readlines()
        for line in lines:
            samples.append([int(x) for x in line.replace(' ', '').replace('\n', '')])
    return np.array(samples)

def load_samples_matching(folder, params):
    files = [f for f in glob.glob(folder + "/*_samples.txt")]
    return_files = []
    if len(files) == 0:
        print(f"Error: no sample files in folder {folder}")

    for f in files: #Search for correct sample files in folder
        json_name = f.replace("True", "true").replace("\'", "\"").replace("=", ":")
        json_name = json_name[json_name.index('{'):json_name.index('}') + 1]
        file_dict = json.loads(json_name)

        if paramsMatch(params, file_dict):
            return_files.append(f)

    #load sample files into memory in parallel
    with joblib.parallel_backend('multiprocessing', n_jobs=max(16, len(return_files))):
        samples_ = joblib.Parallel()(joblib.delayed(loadSamplesFromFile)(f) for f in return_files)

    return np.array(samples_, dtype=int)


def plotDataPoints(runs, xkey, ykey, params, plt=plt_, color_index=0, label=None, linestyle='solid', ynorm=None):
    data = dict()
    valid_runs = [run for run in runs if paramsMatch(params, run)]
    for run in valid_runs:
        data[run[xkey]] = run['results'].get(ykey, np.nan)

    if ynorm is not None:
        for run in valid_runs:
            if ynorm in run['results']:
                norm = np.median(run['results'][ynorm])
            else:
                norm = np.nan
            data[run[xkey]] = np.array(data[run[xkey]])/norm

    color = plt_.rcParams['axes.prop_cycle'].by_key()['color'][color_index]

    xs = sorted(data.keys())
    medians = dict()
    for x in data:
        plt.scatter([x] * len(data[x]), data[x], color=color, alpha=0.3)
        medians[x] = np.nanmedian(data[x])

    plt.plot(xs, [medians[x] for x in xs], color=color, label=label, linestyle=linestyle)
    plt.legend()
    return medians


def plotRun(runs, xkey, ykey, params, seriesKey=None, seriesValues=None, label=None, plt=plt_):

    if seriesKey is not None and seriesKey in params:
        print("Warning: seriesKey in params, may lead to missing data")

    if xkey in params:
        print("Warning: xkey in params, may lead to missing data")

    if seriesValues is None and seriesKey is not None:
        seriesValues = set()
        for run in runs:
            if paramsMatch(params, run):
                seriesValues.add(run[seriesKey])
    else:
        if seriesKey is None:
            seriesKey = list(params.keys())[0]
            seriesValues = [params[seriesKey]]

    for seriesValue in seriesValues:
        xvals = []
        plotData = dict()
        for plotKey in ['mean', 'std', 'median', 'max', 'min']:
            plotData[plotKey] = []

        for run in runs:
            if paramsMatch(params, run) and run[seriesKey] == seriesValue:
                xvals.append(run[xkey])
                for key in plotData:
                    plotData[key].append(run['results'][ykey].get(key, np.nan))

        xvals = np.array(xvals)
        for key in plotData:
            plotData[key] = np.array(plotData[key])

        sorted_indices = np.argsort(xvals)
        xvals = xvals[sorted_indices]
        for key in plotData:
            plotData[key] = plotData[key][sorted_indices]

        if label is None:
            label_show = f'{seriesKey} = {seriesValue}'
        else:
            label_show = label

        # plt.errorbar(xvals, np.abs(yvals), yerr=yerrs, label=f'{seriesKey} = {seriesValue}', ls='none', fmt='o', capsize=4)

        for key in plotData:
            plotData[key][plotData[key] == 0] = np.nan
            #print(key)
            #print(plotData[key])

        if np.isnan(np.array(plotData['median'])).all():
            # mean and std -> bad if data spans multiple orders of magnitude
            label_show += "(Mean)"
            plt.plot(xvals, np.abs(plotData['mean']), label=label_show)
            plt.fill_between(xvals, np.abs(plotData['mean']) - plotData['std'], np.abs(plotData['mean']) + plotData['std'], alpha=0.2)
        else:
            plt.plot(xvals, np.abs(plotData['median']), label=label_show)
            plt.fill_between(xvals, plotData['min'], plotData['max'], alpha=0.2)
        plt.legend()
    #plt.xlabel(xkey)
    #plt.ylabel(ykey)
    #plt.title(str(params))
    #plt.legend()
