import numpy as np
import os
import jax
import jax.ops

import jax.numpy as jnp
import progressbar as pb

#jax.config.update('jax_platform_name', 'cpu')

from source.quantum import povms

def multiChainMC(Ns, state, N, num_chains, burn_in, stride=None, local=True):
    key = jax.random.PRNGKey(np.random.randint(0, 10**8, size=1)[0])

    #log_P = povms.ppp(state)
    #a_s = jax.random.choice(key, 4, shape=(10, N))

    #for _ in pb.progressbar(range(10)):
    #    src.info("logP: " + str(log_P(a_s)[0]))
    povm = povms.default_povm

    if local:
        f = getLocalSamples
    else:
        f = getGlobalSamples

    return f(key, povm.ppp(state), N, num_samples=Ns, burn_in=burn_in, stride=stride, num_chains=num_chains)

def getGlobalSamples(key, log_P=None, N=None, num_samples=None, burn_in=1000, stride=None, num_chains=100):
    key, subkey = jax.random.split(key)

    if stride is None:
        stride = 2*N

    num_samples *= stride

    def getSamplesForBasis(b):
        def step(state, _):
            key_, last_outs, last_log_ps_ = state
            key_, subkey1, subkey2, subkey3 = jax.random.split(key_, 4)
            update_locations = jax.random.choice(subkey1, N, shape=(num_chains,))
            new_outcomes_at_update_locations = jax.random.choice(subkey2, jnp.array([b, 3]), shape=(num_chains,))
            candidates = jax.ops.index_update(last_outs, (jnp.arange(num_chains), update_locations),
                                              new_outcomes_at_update_locations).astype(jnp.int32)
            log_Ps = log_P(candidates)

            randoms = jax.random.uniform(subkey3, shape=(num_chains,))
            pivots = log_Ps - last_log_ps_

            conditions = (pivots >= jnp.log(randoms)).reshape(num_chains, 1)
            conditions = jnp.repeat(conditions, N, axis=1)

            samples_ = jnp.where(conditions, candidates, last_outs)
            new_log_ps = jnp.where(conditions[:, 0], log_Ps, last_log_ps_)

            return (key_, samples_, new_log_ps), samples_

        init_samples = jnp.ones((num_chains, N))*3
        init_log_ps = jnp.log(jnp.ones(num_chains)*1e-15)
        _, samples = jax.lax.scan(step, (subkey, init_samples, init_log_ps), None, length=num_samples//num_chains + burn_in)
        #samples shape = (num_samples//num_chains + burn_in, num_chains, N)
        samples = samples[burn_in::stride, :, :]
        samples = samples.reshape((samples.shape[0] * samples.shape[1], samples.shape[2]))
        return samples[:int(num_samples//stride)]

    return jnp.vstack([getSamplesForBasis(b) for b in [0, 1, 2]])

def getLocalSamples(key, log_P=None, N=None, num_samples=None, burn_in=1000, stride=None, num_chains=100):
    key, subkey = jax.random.split(key)

    if stride is None:
        stride = 2*N

    num_samples *= stride

    def step(state, _):
        key_, last_outs, last_log_ps_ = state
        key_, subkey1, subkey2, subkey3  = jax.random.split(key_, 4)
        update_locations = jax.random.choice(subkey1, N, shape=(num_chains,))
        new_outcomes_at_update_locations = jax.random.choice(subkey2, 4, shape=(num_chains,))
        candidates = jax.ops.index_update(last_outs, (jnp.arange(num_chains), update_locations),
                                          new_outcomes_at_update_locations)
        log_Ps = log_P(candidates)

        randoms = jax.random.uniform(subkey3, shape=(num_chains, ))
        pivots = log_Ps - last_log_ps_

        conditions = (pivots >= jnp.log(randoms)).reshape(num_chains, 1)
        conditions = jnp.repeat(conditions, N, axis=1)

        samples_ = jnp.where(conditions, candidates, last_outs)
        new_log_ps = jnp.where(conditions[:, 0], log_Ps, last_log_ps_)

        return (key_, samples_, new_log_ps), samples_

    init_samples = jax.random.choice(subkey, jnp.arange(4), shape=(num_chains, N))
    init_log_ps = jnp.ones(num_chains)*(-np.infty) #set probability of initial state to 0 (log = -np.infty)
    _, samples = jax.lax.scan(step, (subkey, init_samples, init_log_ps), None, length=num_samples//num_chains + burn_in)
    #samples shape = (num_samples//num_chains + burn_in, num_chains, N)
    samples = samples[burn_in::stride, :, :]
    samples = samples.reshape((samples.shape[0] * samples.shape[1], samples.shape[2]))
    return samples[:int(num_samples//stride)]

def scan(f, init, xs, length=None):
    if xs is None:
        xs = [None] * int(length)
    carry = init
    ys = []
    # N=25, Ns=1000, 2 chains -> 35000 steps -> 2h on nvidia A100
    # same config -> 34h on 48 cpus
    for x in pb.progressbar(xs):
        carry, y = f(carry, x)
        ys.append(y)
    return carry, np.stack(ys)

def loadSamples(file_name, num=None, dephasing=0, absolute_path=False):
    if absolute_path is False:
        path = "../../data/samples/"
        path = os.path.join(os.path.dirname(__file__),
                            path)  # to access the POVMDists folder no matter from where the function is called
    else: path = ""

    samples = []
    with open(os.path.join(path, file_name)) as file:
        lines = file.readlines()
        for line in lines:
            samples.append([int(x) for x in line.replace(' ', '').replace('\n', '')])

    if num is None:
        num = len(samples)

    indices = np.random.choice(len(samples), num, replace=True)
    samples = np.array(samples)[indices]

    if dephasing != 0:
        samples = povms.add_dephasing_to_samples(samples, dephasing)

    return samples
