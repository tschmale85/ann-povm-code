import source
import numpy as np
import os
import json

def binarySearch(params, state, search_param, saveDir):
    search_key, target_key, search_min, search_max, max_steps, target_threshold = search_param

    search_vals = []
    target_results = []

    for i in range(int(max_steps)):
        search_val = int((search_min + search_max) / 2)
        search_vals.append(search_val)
        print(f'--- Trying {search_key} = {search_val} ---')
        params[search_key] = search_val
        result = source.learnWithAverage(params, state, doPrint=True, saveDir=saveDir)
        target_result = np.mean(result[target_key])
        target_results.append(target_result)
        if target_result > target_threshold:
            search_min = search_val
        else:
            search_max = search_val

    params['finalMax'] = search_max
    params['finalMin'] = search_min
    params.pop('actFun', None)
    params['searchVals'] = search_vals
    params['searchTargets'] = target_results
    with open(os.path.join(saveDir, 'searchResult.txt'), 'a') as file:
        file.write(json.dumps(params) + "\n")
