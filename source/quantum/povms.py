import itertools

import numpy

import source
from source.quantum import opsListToBigOp, _flatten, dms
from source.quantum import paulis, one, pureStateToDM
from source import util
import jax.numpy

np = numpy

class POVM:
    def __init__(self, povm_operators=None):
        if povm_operators is None:
            povm_operators = pauli4POVM()
        elif type(povm_operators) == str:
            if povm_operators == "sic":
                povm_operators = sic_povm()
            else:
                povm_operators = pauli4POVM(povm_operators)
        self.M = np.array(povm_operators)

        try:
            self.M_inv, self.inv_overlap, self.overlap = self._povm_inv()
        except numpy.linalg.LinAlgError:
            print("POVM is not invertible! Functionality may be severely limited!")

    def __len__(self):
        return len(self.M)

    def _povm_inv(self):
        """
            Returns the povm that is already contracted with the inverse overlap matrix.
        """
        singleOverlap = np.einsum('ikl,jlk', self.M, self.M)  # T_aa' = Tr[M_a@M_a']
        singleInvOverlap = np.linalg.inv(singleOverlap)
        singlePOVM_T = np.einsum('ikl, ij', self.M, singleInvOverlap)
        return singlePOVM_T, singleInvOverlap, singleOverlap

    def sampleToOp(self, sample):
        Ms = self.M[np.array(sample)]
        return opsListToBigOp(Ms)

    def logP_rho(self, a, rho):
        """
            Computes POVM log-Probabilities of a density matrix for a singe povm outcome
        Parameters
        ----------
        a: 1-D array consisting of single particle povm outcomes
        rho: 1-D array describing the pure state

        Returns
        -------
        single POVM log-probability
        """
        a = a[::-1]
        N = len(a)
        rho_t = dms.opToTensorOp(rho)
        einsum_args = []
        for i in range(N):
            einsum_args.append(self.M[a[i]])
            einsum_args.append([i, N+i])
        P = np.real(np.einsum(rho_t, range(2*N), *einsum_args, optimize=True))
        return np.log(P)

    def logP(self, a, pure_state=None, dephasing=0):
        """
            Computes POVM log-Probabilities of a state for a singe povm outcome
        Parameters
        ----------
        a: 1-D array consisting of single particle povm outcomes
        pure_state: 1-D array describing the pure state
        dephasing: additional dephasing noise

        Returns
        -------
        single POVM log-probability
        """
        a = a[::-1]
        N = len(a)
        pure_state_t = pure_state.reshape(*[2]*N)
        einsum_args = []
        for i in range(N):
            einsum_args.append(self.M[a[i]])
            einsum_args.append([i, N+i])
        P = np.real(np.einsum(pure_state_t.conj(), range(N), *einsum_args, pure_state_t, range(N, 2*N), optimize=True))
        if dephasing == 0:
            return np.log(P)
        else:
            uniform = np.prod(np.array([np.trace(self.M[a[i]]).real/2 for i in range(N)]))
            return np.log(dephasing*uniform + (1-dephasing)*P)

    def ppp(self, pure_state=None, dm=None, batch_size=None, do_jit=True, dephasing=0, N=None):
        """
            'Parallel Probability Processor' - returns a (partially) vectorized version of P(a)
        Parameters
        ----------
        pure_state: 1-D array describing the pure state (only if dm is None)
        dm: 2-D array describing a density matrix (only if pure_states is None)
        batch_size: sets the number of probabilities to be computed via vector-instructions
        do_jit: Whether or not to jit the output function
        dephasing: additional dephasing noise (only for pure states)
        N: particle number. only needed if dm given

        Returns
        -------
        function that computes POVM log-probabilities in batches
        """
        def P_vec(_, a): # _ for lax.scan
            return _, self.logP(a, pure_state, dephasing=dephasing)

        def P_vec_dm(_, a): # _ for lax.scan
            return _, self.logP_rho(a, dm)

        if pure_state is not None:
            P_vec = jax.vmap(P_vec) # vectorize
            N = int(np.log2(len(pure_state)))
        elif dm is not None:
            P_vec = jax.vmap(P_vec_dm)
            #N = int(np.log2(len(dm[0]))) #cant infer shape, because then jax cant take derivatives...

        if batch_size is None:
            if N < 20:
                batch_size = 1000
            else:
                batch_size = 10

        def Ps(ai):
            total_len = len(ai)
            #ai = ai.reshape(max(total_len//batch_size, 1), min(batch_size, total_len), N) # Create batches
            ai = np.array(np.split(ai, max(1, total_len//batch_size)))
            return jax.lax.scan(P_vec, None, ai)[1].reshape(total_len)

        if do_jit:
            return jax.jit(Ps)
        else: return Ps

    def purity(self, dist_t, N=None):
        if N is None:
            N = dist_t.ndim

        einsum_parts = itertools.chain(*zip([self.inv_overlap] * N, [[i, i + N] for i in range(N)]))
        return util.safeReal(
            np.einsum(dist_t, range(N), dist_t, range(N, 2 * N), *list(einsum_parts), [], optimize=True))

    def sample_purity(self, samples, N=None):
        if N is None:
            N = len(samples[0])

        samplesA = np.array(samples[:len(samples) // 2])
        samplesB = np.array(samples[len(samples) // 2:])
        res = np.ones(len(samples) // 2, dtype=source.dtypeR)
        for i in range(N):
            samples = np.vstack((samplesA[:, i], samplesB[:, i])).T
            res *= self.inv_overlap[tuple([samples[:, i] for i in [0, 1]])].real
        return util.safeReal(np.mean(res)), util.safeReal(np.std(res) / np.sqrt(len(samplesA)))

    def getPovmUnitary_2ptcl(self, op_t: np.ndarray):
        """
            Converts a 2 particle operator in tensor representation (i.e. from dms.opToTensorOp) to the POVM formalism

        Parameters
        ----------
        op_t: 2 particle operator in tensor representation (i.e. from dms.opToTensorOp), shape (2, 2, 2, 2)

        Returns
        -------
        operator in povm formalism, shape (4, 4, 4, 4)
        """

        # return np.einsum('ABCD, aAG, dBH, EFGH, gCE, jDF->gjad',
        #                 op_t, pauli4POVM, pauli4POVM, op_t.T.conj(), pauli4POVM_inv, pauli4POVM_inv, optimize=True)
        return np.einsum('ABCD, aCE, dDF, FEGH, gHA, jGB->gjad',
                         op_t, self.M_inv, self.M_inv, op_t.T.conj(), self.M, self.M, optimize=True)

    def getPovmObservable(self, op_tensor):
        """
            Turns an operator in tensor representation into a povm operator in tensor representation,
            i.e. computes T_ij^(-1)Tr[M_j O] for an observable O, a POVM element M_j and the inverse overlap
            matrix T_ij^(-1). The original representation would be obtained via O = T_ij^(-1)Tr[M_j O] M_i.

            Args:
                op_tensor: operator in tensor representation, e.g. obtained via dms.opToTensorOp()
        """

        N = op_tensor.ndim // 2
        op_tensor = dms.subtranspose(op_tensor).conj()
        einsum_input = _flatten([(self.M_inv, [2 * N + i, i, N + i]) for i in range(N)])
        # TODO: Explicit einsum output shape, otherwise function will fail for large input dim!!!
        return np.einsum(op_tensor, range(2 * N), *einsum_input, optimize='greedy')

    def get_single_ptcl_distribution(self, rho_single):
        """
        Same as getPOVMDistribution but only for 2x2 density matrices (which is this easier for jax to work with
        as no reshaping is necessary)
        Parameters
        ----------
        rho_single: 2x2 matrix

        """
        return np.einsum('ij, kji', rho_single, self.M).real

    def getPOVMDistribution(self, rho, return_1D=True):
        """
            Calculates the pauli povm probability distribution for a given density matrix (rho).

            Parameters:
                rho: A square density matrix

                return_1D: if True, return a 1D probability distribution. If False, return a tensor
                           representation of the distribution
        """
        rho_tensor = dms.subtranspose(dms.opToTensorOp(rho))

        N = rho_tensor.ndim // 2

        einsum_input = _flatten([(self.M, [2 * N + i, N + i, i]) for i in range(N)])
        einsum_output = [2 * N + i for i in range(N)]
        # print(np.einsum_path(rho_tensor, range(2 * N), *einsum_input, optimize='greedy')[1])
        tensor_dist = np.einsum(rho_tensor, range(2 * N), *einsum_input, einsum_output, optimize='greedy')
        tensor_dist = util.safeReal(tensor_dist)
        #tensor_dist[tensor_dist < 0] = 0

        if return_1D:
            return self.tensorDistTo1D(tensor_dist)
        else:
            return tensor_dist

    def entanglement_entropy(self, dist_t):
        rho_half = self.linRecon_t(dist_t)
        return dms.vonNeumannEntropy(rho_half)

    def linRecon_t(self, dist_t, return_matrix=True, printOptimization=False):
        """
            Converts a povm probability distribution into a density matrix, for the Pauli-4 POVM.
            The povm distribution must be passed as a tensor with N single particle indices.

            Parameters:
                dist_t: A tensor representation of the povm probability distribution

                return_matrix: If True, return a square density matrix. If False, return density matrix in
                               tensor representation
                printOptimization: If True, print the optimizations done by the einsum function.
        """
        N = dist_t.ndim

        einsum_input = _flatten([(self.M_inv, [i, N + i, 2 * N + i]) for i in range(N)])
        einsum_output = [N + i for i in range(N)] + [2 * N + i for i in range(N)]

        if printOptimization:
            path_info = np.einsum_path(dist_t, range(N), *einsum_input, einsum_output, optimize=True)
            print(path_info[1])

        '''
            povm_tensor has N indices, over all of which need to be summed (range(N)).
            The next arguments are N copies of singlePOVM_T which are all of shape (4, 2, 2).
            Here, the first index needs to be contracted with an index of povm_tensor.
            Following this tensor diagram https://arxiv.org/pdf/1912.11052.pdf
        '''
        tensorDM = dms.subtranspose(np.einsum(dist_t, range(N), *einsum_input, einsum_output, optimize=True))
        if return_matrix:
            return dms.tensorOpToOp(tensorDM)
        else:
            return tensorDM

    def linRecon(self, povm_distribution, N=None, return_matrix=True, printOptimization=False):
        """
            Converts a povm probability distribution into a density matrix, for the Pauli-4 POVM.

            Parameters:
                povm_distribution: A 1D povm probability distribution
                N: particle number
                return_matrix: If True, return a square density matrix. If False, return density matrix in
                               tensor representation
                printOptimization: If True, print the optimizations done by the einsum function.
        """
        if N is None:
            N = int(np.log(len(povm_distribution))/np.log(len(self)))
        povm_tensor = self.dist1DToTensor(povm_distribution, N)
        return self.linRecon_t(povm_tensor, return_matrix=return_matrix, printOptimization=printOptimization)

    def getNPtclePOVM(self, N):
        """
            Return 4^N N-particle pauli povm operators
        """
        res = []
        for ops in itertools.product(self.M, repeat=N):
            res += [opsListToBigOp(ops)]
        return res

    def getSamplesFromPOVMDist(self, dist, num):
        N = int(numpy.log(len(dist))/numpy.log(len(self)))

        #np.random.seed(0)
        samples = numpy.random.choice(range(len(dist)), num, p=dist)

        def str_base(number):
            for i in range(N):
                number, digit = divmod(number, len(self))
                yield digit

        dataset = []
        for s in samples:
            dataset.append(list(str_base(s)))
        return dataset

    def add_dephasing_to_samples(self, samples, prob):
        samples = numpy.array(samples)
        num = len(samples)
        num_samples_to_replace = int(prob * num)
        sample_indices_to_replace = numpy.random.choice(num, size=num_samples_to_replace, replace=False).astype(numpy.int32)

        uniform_probs = np.array(self.getPOVMDistribution(np.eye(2)))

        new_samples = numpy.random.choice(4, size=(num_samples_to_replace, len(samples[0])),
                                          p=uniform_probs/numpy.sum(uniform_probs))
        samples[sample_indices_to_replace] = new_samples
        return np.array(samples)

    def dist1DToTensor(self, dist, N):
        """
            Converts a one-dimensional povm distribution into a tensor of shape (4, 4, ..., 4)
        """
        return dist.reshape(*[len(self) for _ in np.arange(N)]).T  # Todo: rewrite all einsums so that final .T can be removed

    def tensorDistTo1D(self, tensor_dist):
        """
            Converts a tensor of shape (4, 4, ..., 4) into a one-dimensional povm distribution
        """
        N = tensor_dist.ndim
        return tensor_dist.T.reshape(len(self) ** N)  # Todo: rewrite all einsums so that .T can be removed

    def tensorPovmUnitaryToMatrix(self, povmUnitary_t):
        N = povmUnitary_t.ndim // 2
        return povmUnitary_t.reshape((len(self) ** N, len(self) ** N))

    def povmInputVecToIndex(self, vec):
        """
            Converts a povm outcome vector with single particle outcomes (eg. [0, 2, 2, 1, 3, 3, 0])
            to a one dimensional index ranging from 0 to 4**len(vec)-1
        """
        return int(''.join(map(str, vec[::-1])), base=len(self))

    @staticmethod
    def partialTrace(dist_t, remaining_sites):
        N = dist_t.ndim
        return np.einsum(dist_t, range(N), remaining_sites)

    @staticmethod
    def sampleObs(obs, samples, axes):
        """
        Evaluates an observable from samples of a povm distribution

        Parameters
        ----------
        obs: povm_observable, 1-D, like from getPovmObservable()
        samples: samples from a povm distribution, shape (num_samples, num_particles)
        axes: list of particle indices on which to evaluate the observable

        Returns
        -------
        expectation value, std. deviation
        """

        if obs.ndim != len(axes):
            raise Exception('Observable shape does not match axes!')

        samples = np.array(samples)
        # evaluate observable at sample locations
        evaluated_obs = obs[tuple([samples[:, i] for i in axes])]
        mean = np.mean(evaluated_obs)
        var = np.mean(evaluated_obs ** 2) - mean ** 2
        return util.safeReal(mean), util.safeReal(np.sqrt(var / len(evaluated_obs)))

    @staticmethod
    def applyUnitary(dist_t: np.ndarray, op_t, sites: list):
        """

        Parameters
        ----------
        dist_t: povm distribution in tensor representation, shape (4, 4, ..., 4)
        op_t: a povm operator (eg. from getPovmUnitary_2ptcl())
        sites: an array of single particle sites on which to apply the unitary, eg [1, 5] for a 2 particle operator

        Returns
        -------
        a new povm distribution with the same shape as dist_t
        """

        if op_t.ndim // 2 != len(sites):
            raise Exception(f"Refusing to apply a {op_t.ndim // 2}-particle operator on {len(sites)} sites...")

        sites = [int(x) for x in sites]

        N = dist_t.ndim
        n_sites = len(sites)

        #abcd, 6d4c21 -> 12a4b6

        out_shape = np.arange(N)
        op_shape = list(range(N, N+2*n_sites))
        #print(op_shape[:n_sites], op_shape[n_sites:])
        out_shape[sites] = op_shape[:n_sites]
        out_shape = out_shape[::-1]
        dist_shape = np.arange(N)
        #temp = op_shape[n_sites:]
        dist_shape[sites] = op_shape[n_sites:]
        dist_shape = dist_shape[::-1]

        dist_shape = [int(x) for x in dist_shape]
        out_shape = [int(x) for x in out_shape]

        #print(op_shape, dist_shape, out_shape)

        #print(np.einsum_path(op_t, op_shape, dist_t, dist_shape, out_shape)[1])
        return util.safeReal(np.einsum(op_t, op_shape, dist_t, dist_shape, out_shape))

    @staticmethod #Todo: make sites argument match with qutips (currently [shifted by one - NOT ANYMORE] and reversed)
    def expVal(dist_tensor, povm_op_tensor, sites):
        """
            Computes the expectation value of the povm operator acting given sites in the state dist_tensor.

            Args:
                dist_tensor: A povm distribution in tensor representation, e.g. obtained via dist1DToTensor()

                povm_op_tensor: A operator in povm tensor representation, e.g. obtained via getPovmObservable()

                sites: The sites the operator should act on, e.g. [3, 4] for a 2-particle operator.

            Returns:
                float: Expectation value
        """

        if povm_op_tensor.ndim != len(sites):
            raise Exception(f"Refusing to apply a {povm_op_tensor.ndim}-particle operator on {len(sites)} sites...")

        # np.array(sites) - 1 does not work, even if all data types are set to int/int32... maybe bug in numpy?
        #sites = [x - 1 for x in sites]

        #print(np.einsum_path(dist_tensor, range(dist_tensor.ndim), povm_op_tensor, sites, [], optimize='greedy')[1])
        return util.safeReal(np.einsum(dist_tensor, range(dist_tensor.ndim), povm_op_tensor, sites, [], optimize=True))


def pauli4POVM(rank1_outcomes='+++'):
    """
        Returns the 4 single particle pauli povm operators (2x2 matrices)
    """
    rank1_outcomes = '+' + rank1_outcomes

    vecs = []
    for i, pauli in enumerate(paulis):
        eigenvals, eigenvecs = numpy.linalg.eigh(pauli)
        index = 1
        if rank1_outcomes[i] == '+':
            index = 0
        if eigenvals[0] > 0:
            vecs.append(eigenvecs[:, index])  # select the eigenvector with positive eigenvalue for '+'
        else:
            vecs.append(eigenvecs[:, 1 - index])

    # construct POVM elements
    M = [1. / 3 * pureStateToDM(vecs[i]) for i in [1, 2, 3]]
    M.append(one - sum(M))
    return numpy.array(M)

def sic_povm():
    states = numpy.array([
        [1, 0],
        [1 / numpy.sqrt(3), numpy.sqrt(2 / 3)],
        [1 / numpy.sqrt(3), numpy.exp(2j / 3 * numpy.pi) * numpy.sqrt(2 / 3)],
        [1 / numpy.sqrt(3), numpy.exp(4j / 3 * numpy.pi) * numpy.sqrt(2 / 3)],
    ])
    projectors = numpy.array([pureStateToDM(state) for state in states])
    return projectors/2

def pauli6_povm():
    pauli6 = []
    for pauli in paulis[1:]:
        evals, evecs = np.linalg.eigh(pauli)
        pauli6.append(pureStateToDM(evecs[:, 0]))
        pauli6.append(pureStateToDM(evecs[:, 1]))
    pauli6 = np.array(pauli6)/3
    return pauli6


default_povm = POVM(pauli4POVM('+++'))

def __getattr__(name):
    if name == "getPauli4Dist":
        return getattr(default_povm, "getPOVMDistribution")
    elif name == "getNPtclePauli4":
        return getattr(default_povm, "getNPtclePOVM")

    default_povm_function = getattr(default_povm, name, None)
    if default_povm_function is not None and callable(default_povm_function):
        #print(f'Calling {name} on old package!')
        return default_povm_function
    else:
        raise Exception(f'Function {name} not implemented')
