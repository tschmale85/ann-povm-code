import itertools

import numpy as np

from source.util import safeReal
import source
import scipy.sparse as sp_sparse
from source.quantum import dms

one = np.array([[1, 0], [0, 1]], dtype=source.dtypeC)
sx = np.array([[0, 1], [1, 0]], dtype=source.dtypeC)
sy = np.array([[0, -1j], [1j, 0]], dtype=source.dtypeC)
sz = np.array([[1, 0], [0, -1]], dtype=source.dtypeC)

c_z = np.eye(4)
c_z[3, 3] *= -1

paulis = [one, sx, sy, sz]

up = np.array([1, 0], dtype=source.dtypeC)
down = np.array([0, 1], dtype=source.dtypeC)
plus = (up + down)/np.sqrt(2)
minus = (up - down)/np.sqrt(2)

bell = 1/np.sqrt(2)*(np.kron(up, up) + np.kron(down, down))

def productState(single_ptcl_state, N):
    """
        Tensors N copies of single_ptcl_state

    Parameters
    ----------
        single_ptcl_state: single particle pure state vector
        N: desired product state copies to produce

    Returns
    -------
        single_ptcl_state (x) single_ptcl_state (x) ... (x) single_ptcl_state
    """
    res = single_ptcl_state
    for _ in range(N-1):
        res = np.kron(res, single_ptcl_state)
    return res

def product_state_from_list(single_particle_states):
    res = 1
    for state in single_particle_states:
        res = np.kron(res, state)
    return res

def GHZ(N):
    ghz = (productState(up, N) + productState(down, N))/np.sqrt(2)
    return pureStateToDM(ghz)

def pureStateToDM(state):
    """
        Converts pure state vector to a pure state density matrix
    """
    return np.outer(state, state.conj())


def expVal(op, psi, sites=None, dephasing=0):
    """
        Computes the expectation value of op in the pure state
        (or density matrices of the form (1-dephasing)|psi><psi| + dephasing/2**N*Identity)
        If sites is given, psi may also be a list of states in of which to compute the expectation value.

    Parameters
    ----------
    op: shape=(2**N, 2**N) or smaller if sites is given
    psi: shape=(2**N) or (..., 2**N) if sites is given
    sites: List of sites on which to apply the operator
    dephasing: degree of mixture
    """
    N = int(np.log2(psi.shape[-1]))
    if sites is None:
        res = safeReal(np.conj(psi)@op@psi/(np.conj(psi)@psi))
    else:
        if len(sites) != int(np.log2(op.shape[0])):
            raise Exception(f"Refusing to apply {int(np.log2(op.shape[0]))}-site operator to {len(sites)} sites...")

        out_sites = list(range(N, N + len(sites)))
        op_indices = sites + out_sites

        shape_0 = psi.shape
        state_t = psi.reshape(*(list(shape_0[:-1]) + [2] * N))
        op = op.reshape(*[2] * (2 * len(sites)))

        second_indices = np.arange(N)
        second_indices[sites] = out_sites
        second_indices = [int(x) for x in second_indices]
        res = np.einsum(state_t.conj(), [...] + list(range(N)), op, op_indices, state_t, [...] + second_indices,
                        optimize=True).real
    if dephasing == 0:
        return res
    else:
        op = op.reshape((2**len(sites), 2**len(sites)))
        return (1-dephasing)*res + dephasing*np.trace(op).real*(1/2)**len(sites)


def subsystem_dm(state, num):
    """
            Traces out subsystem on a pure state
        Parameters
        ----------
        state: 1-D pure state vector
        num: size of subsystem of which to compute purity (default: N//2)

        Returns
        -------
        subsystem density matrix
        """
    N = int(np.log2(len(state)))

    if num is None:
        num = N // 2

    state_t = state.reshape(*[2] * N).T
    dm_t = np.einsum(state_t, range(N), state_t.conj(), list(range(N, N + num)) + list(range(num, N)),
                     optimize=True)
    return dm_t.reshape(2 ** num, 2 ** num)

def subsystem_purity(state, dephasing=0, num=None):
    """
        Fast Half chain Purity computation for pure states
        (or density matrices of the form (1-dephasing)|psi><psi| + dephasing/2**N*Identity)
    Parameters
    ----------
    state: 1-D pure state vector
    dephasing: degree of mixture
    num: size of subsystem of which to compute purity (default: N//2)
    """
    dm = subsystem_dm(state, num)
    return (1-dephasing)**2*dms.purity(dm) + (2*dephasing*(1-dephasing) + dephasing**2)/2**num


#liftOP(A, 2, 5) -> 1(x)1(x)A(x)1(x)1
def liftOp(op, position, numSpins, sparse=False):
    position += 1
    res = 1

    if sparse:
        op = sp_sparse.csr_matrix(op)

        for i in range(position - 1):
            res = sp_sparse.kron(res, np.identity(2), format='csr')
        res = sp_sparse.kron(res, op, format='csr')
        for i in range(position + 1, numSpins + 1):
            res = sp_sparse.kron(res, np.identity(2), format='csr')
        return res

    for i in range(position-1):
        res = np.kron(res, np.identity(2))
    res = np.kron(res, op)
    for i in range(position+1, numSpins+1):
        res = np.kron(res, np.identity(2))
    return res

#return op(x)1(x)...(x)1 + 1(x)op(x)1(x)...(x)1 + ... with N total operators
def totalOp(op, N, sparse=False):
    return sum([liftOp(op, i, N, sparse=sparse) for i in range(0, N)])


def opsListToBigOp(ops):
    res = 1
    for op in ops:
        res = np.kron(res, op)
    return res


def countOccurrences(data, expectedUniqueEntries):
    result = np.zeros(expectedUniqueEntries)
    for d in data:
        result[d] += 1
    return result


def _flatten(x):
    return list(itertools.chain.from_iterable(x))
