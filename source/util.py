import numpy
import scipy.integrate as solver
import progressbar as pb
import inspect
import matplotlib.pyplot as plt
import source

np = numpy

warn = True

def safeReal(x):
    if warn and np.any(np.abs(x.imag) > np.array(source.errorMargin)):
        source.warn(str(inspect.stack()[1].function) + f": Big imaginary part discarded ({np.max(np.abs(x.imag)):.2e})!")
    return x.real

def safeAbs(x: np.ndarray):
    assert (x.imag == 0).all()

    if (x < -source.errorMargin).any():
        source.warn(str(inspect.stack()[1].function) + f": Big negative part discarded ({np.min(x):.2e})!")

    return np.abs(x)

def isClose(a, b):
    return np.max(np.abs(a-b)) < source.errorMargin
    
    
def fidelity(P, Q):
    return np.sum(np.sqrt(P*Q))**2

def getLastAvgSlope(y, doPlot=False):
    y = np.array(y)
    start = int(len(y)*0.8)#len(y) - 400
    if start < 0:
        return np.nan, np.nan
    x = np.arange(start, len(y))
    y = y[start:]
    
    try:
        #minima_positions = np.r_[True, y[1:] < y[:-1]] & np.r_[y[:-1] < y[1:], True]
        
        #if len(minima_positions) != len(y) or len(minima_positions) != len(x):
        #    return np.nan, np.nan
        #y = y[minima_positions]

        #x = x[minima_positions]

        if len(x) != len(y) or len(y) < 2 or len(x) < 2:
            return np.nan, np.nan

        m, b = np.polyfit(x, y, 1)

        if doPlot:
            plt.plot(x, m*x+b)
        
    except:
        import sys
        print(sys.exc_info()[0])
        return np.nan, np.nan

    return m, b

def getDatasetWeights(dataset, return_dist=False, povm_size=4):
    """
        getDatasetWeights(dataset)[i] is the relative frequency of the i'th entry of the dataset
    """
    frequency_dist_d = dict()
    rel_probs = np.empty(len(dataset))
    for d in dataset:
        d = tuple(d)
        if d in frequency_dist_d:
            frequency_dist_d[d] += 1
        else:
            frequency_dist_d[d] = 1
    for key in frequency_dist_d:
        frequency_dist_d[key] /= len(dataset)
    for i in range(len(dataset)):
        rel_probs[i] = frequency_dist_d[tuple(dataset[i])]

    if return_dist:
        N = len(dataset[0])
        dataset_dist_t = np.zeros(shape=[povm_size for _ in range(N)])
        for k in frequency_dist_d:
            dataset_dist_t[k] = frequency_dist_d[k]
        return rel_probs, dataset_dist_t
    return rel_probs
