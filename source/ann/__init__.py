import jax.numpy as jnp
from jax import random
import jax
import flax
import time
import itertools
from source.ann import Networks, AutoregressiveNNs
from source import sampling
from functools import partial
from jax import jit

encoding_dimension = {
    'image': 1,
    'binary': 2,
    'one_hot': 4
}

def get_net(params):
    net_name = params['net']

    if "AR" in net_name:
        Network = getattr(AutoregressiveNNs, net_name)
    else:
        Network = getattr(Networks, net_name)

    layers = params['l']

    args = {
        'features': (params['f'],) * layers,
        'kernels': (params['k'],) * layers,
        'actFns': (params['actFun'],) * layers,
        'encoding': params['encoding']
    }

    if "2D" in net_name:
        args['L'] = int(jnp.sqrt(params['N']))

    return Network(**args)

def getBasis(N):
    basis = []
    for label in itertools.product(range(4), repeat=N):
        basis += [label]
    basis = jnp.array(basis)
    return basis

def encode(x, encoding='one_hot'):
    if encoding == 'one_hot':
        x = jax.nn.one_hot(x, 4)
    if encoding == 'image':
        x = jnp.expand_dims(x, -1)
    return jnp.expand_dims(x, axis=-1)


def create_optimizer(net, params, initial_params=None):
    opt = params.get('opt', flax.optim.Adam)

    opt_params = {'learning_rate': params['lr']}
    if 'wd' in params:
        opt_params['weight_decay'] = params['wd']

    if 'beta1' in params:
        opt_params['beta1'] = params['beta1']

    if 'beta2' in params:
        opt_params['beta2'] = params['beta2']

    #optimizer, _ = ann.create_optimizer(net, N, opt, opt_params=opt_params, seed=int(time.time() * 100000),
    #                                    initial_params=initial_params)
    rng = random.PRNGKey(int(time.time() * 100000))
    rng, init_rng = random.split(rng)

    init_shape = jnp.ones((1, params['N']), jnp.float32)

    if initial_params is None:
        initial_params = net.init(init_rng, init_shape)['params']
    params = initial_params
    optimizer_def = opt(**opt_params)
    optimizer = optimizer_def.create(params)
    return optimizer, params

def getNumParams(optimizer):
    return sum(jax.tree_leaves(jax.tree_map(jnp.size, optimizer.target)))

def numParams_CNN(l, f, N, k, encoding='image'):
    return l*f + k*f + (l-1)*k*f*f + 1 + N*f + (encoding_dimension[encoding]-1)*k*f

def numParams_CCNN(l, f, N, k, encoding='image'):
    #bias + kernel_1 + kernel_rest + bias_final + fully_connected_final + encoding_dependant_first_kernel
    return l*f + k*f + (l-1)*k*f*f + 1 + f + (encoding_dimension[encoding]-1)*k*f

@partial(jit, static_argnums=(0, 2, 3))
def getSamples(network, network_params, N, num_samples, key, discard=2**13): #2**13 ~ 8k

    if type(network) == AutoregressiveNNs.ARCNN:

        def genBatch(state, _):
            key_ = state
            key_, subkey = jax.random.split(key_)

            return key_, AutoregressiveNNs.sample(network, subkey, network_params, N, 10000)

        key, samples = jax.lax.scan(genBatch, key, None, int(num_samples//10000))
        return jnp.array(samples).reshape(-1, N)

    def P(a):
        return network.apply({'params': network_params}, a).squeeze()

    #return sampling.getSamples(key, P, N, num_samples, discard, 10)
    return sampling.getLocalSamples(key, P, N, num_samples, discard, 16)
