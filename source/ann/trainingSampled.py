import time

import flax.linen
import jax
import numpy as onp
from jax import random, numpy as jnp
from functools import partial
from jax import jit
from source import util

import source as src
from source import ann
from source.quantum import povms

net: flax.linen.Module

#from jax.config import config
#config.update('jax_debug_nans', True)

@jit
def evaluate(params, samples, weights, target):
    res = net.apply({'params': params}, samples).squeeze()
    res = jnp.exp(res)

    if net.requires_norm:
        res /= jnp.nanmean(res / weights)#Compute Monte Carlo estimate of normalization constant

    infid = 1-jnp.nanmean(jnp.sqrt(res*target)/weights) #Compute Monte Carlo estimate of loss function
    return infid, res

def normalizeNetworkOutputs(net_outs, N):
    """
    Normalizes network log-probs, when given log-probs = network(samples) for samples ~ network

    Parameters
    ----------
    net_outs: log-probs = network(samples) for samples ~ network
    N: particle number

    Returns
    -------
    normalized log-probs
    """
    net_probs = jnp.exp(-net_outs)
    log_norm = N * jnp.log(4) - jnp.log(jnp.nanmean(net_probs))
    return net_outs - log_norm # P(a) = 1/4**N * <1/NN(a)>_{a~NN(a)} * NN(a)


@jit
def evaluateFromNetworkSamples(params, samples, log_target, N):
    res = net.apply({'params': params}, samples).squeeze()

    if net.requires_norm:
        res = normalizeNetworkOutputs(res, N)

    infid = 1-jnp.nanmean(jnp.exp((log_target-res)/2)) #Compute Monte Carlo estimate of loss function

    return infid, res


def train_step(N, optimizer, batch, weights, target, norm_batch, max_grad_mag):
    """Train for a single step."""
    def loss_fn(params):
        net_log_prob = net.apply({'params': params}, batch).squeeze()

        if norm_batch is not None: #Norm batch given -> use it to estimate norm. Else -> no need to normalize
            #Dont compute normalization of probability distribution using samples from dataset -> better generalization

            norm_batch_res = jnp.exp(net.apply({'params': params}, norm_batch)).squeeze()
            log_norm = jnp.log(jnp.mean(norm_batch_res)) + N*jnp.log(4)

            net_log_prob -= log_norm

        return -jnp.mean(net_log_prob) #-jnp.mean(target*net_log_prob/weights)
    grad_fn = jax.value_and_grad(loss_fn, has_aux=False)
    loss, grad = grad_fn(optimizer.target)

    def clip(x):
        norm = jnp.linalg.norm(x)
        return jnp.where(norm < max_grad_mag, x, x/norm*max_grad_mag)

    if max_grad_mag != -1:
        grad = jax.tree_map(clip, grad)

    grad_mag = jnp.mean(jnp.array(jax.tree_leaves(jax.tree_map(jnp.linalg.norm, grad)))) #compute some length of grad

    optimizer = optimizer.apply_gradient(grad)
    return optimizer, (loss, grad_mag)

@partial(jit, static_argnums=(4, 6, 7))
def train_epoch(optimizer, dataset, weights, target, batch_size, key, N, max_grad_mag):

    def iterable(state, _):
        key_, opt = state
        key_, subkey = jax.random.split(key_)
        #Select random samples from dataset
        batch_indices = jax.random.choice(subkey, len(dataset), shape=(batch_size,), replace=True)

        norm_batch = None
        if net.requires_norm:
            key_, subkey = jax.random.split(key_)
            #Select random samples from entire basis
            norm_batch = jax.random.choice(subkey, 4, shape=(batch_size, N), replace=True)

        opt, metrics = train_step(N, opt, dataset[batch_indices], weights[batch_indices],
                                  target[batch_indices], norm_batch, max_grad_mag)
        return (key_, opt), metrics

    if len(dataset) < batch_size:
        batch_size = len(dataset)

    num_steps_per_epoch = len(dataset)//batch_size
    (key, optimizer), epoch_metrics = jax.lax.scan(iterable, (key, optimizer), None, length=num_steps_per_epoch)

    #epoch_metrics = jnp.sum(epoch_metrics)
    epoch_metrics = jnp.array(epoch_metrics)

    epoch_loss = epoch_metrics[0]
    epoch_grads = epoch_metrics[1]

    return optimizer, (jnp.mean(epoch_loss), jnp.mean(epoch_grads)), key


basis = []


def learn(params, dataset, target, distribution_loggers, physical_loggers, doPrint=False,
          initial_params=None, return_nn_params=False):

    src.info(f"Devices: {jax.devices()}")

    dataset_weights = util.getDatasetWeights(dataset)
    dataset_weights = jnp.array(dataset_weights)
    dataset = jnp.array(dataset)

    dataset_entropy = jnp.mean(jnp.log(dataset_weights))

    N = params['N']

    global net
    net = ann.get_net(params)

    haltLoss = params.get('halt_loss', -100)
    maxEpochs = params.get('maxEpochs', 10 ** 5)
    halt_slope = params.get('halt_slope', 0)
    batch_size = params.get('batch_size', 4 ** params['N'])

    optimizer, _ = ann.create_optimizer(net, params, initial_params=initial_params)

    src.info(f"{ann.getNumParams(optimizer)} parameters for {4 ** N} POVM elements"
             f" (Overfit = {ann.getNumParams(optimizer) / 4 ** N}) using {len(dataset)} samples")
    src.info(f'Epoch\tLikelihood\t|Grad|\tDataset-Infid\tPOVM-Infid-Slope\tPOVM Infid')

    key = random.PRNGKey(int(time.time()))

    start_time = time.time()

    basis_weights = None
    global basis
    if not callable(target):
        target = jnp.array(target)
        basis = jnp.array(ann.getBasis(N))
        basis_weights = (jnp.ones(len(basis)) / len(basis))

    i = 0
    for i in range(maxEpochs):
        #print('Epoch')
        #if not callable(target):
        #    optimizer, metrics, key = train_epoch(optimizer, basis, basis_weights, target, batch_size, key, N)
        #else:
        optimizer, (metrics, grads), key = train_epoch(optimizer, dataset,
                                                       dataset_weights, dataset_weights, batch_size, key, N,
                                                       params['gc'])

        metrics = jax.device_get(jnp.abs(metrics)) + dataset_entropy
        grads = jax.device_get(grads)

        distribution_loggers['loss'].add_value('NN', metrics)
        distribution_loggers['grad'].add_value('NN', grads)

        if i % 80 == 0: #Evaluate
            #print('Evaluate')
            loss, nn_dist = evaluate(optimizer.target, dataset, dataset_weights, dataset_weights)
            loss = jax.device_get(loss)

            if not callable(target):
                povm_infid, full_dist = evaluate(optimizer.target, basis, basis_weights, target)
                povm_infid = jax.device_get(povm_infid)

                full_dist_t = povms.dist1DToTensor(full_dist / onp.sum(full_dist), N)

                for logger in physical_loggers:
                    logger.log('NN', distribution=full_dist_t)
            else:
                key, subkey = jax.random.split(key)
                src.info('Sampling')
                network_samples = jax.device_get(ann.getSamples(net, optimizer.target, N,
                                                                params['NsEval'], subkey))
                src.info('Computing Probs')

                povm_infid = -1
                if N < 11:
                    true_log_probs = target(network_samples)
                    povm_infid, nn_on_samples = evaluateFromNetworkSamples(optimizer.target, network_samples,
                                                                           true_log_probs, N)
                for logger in physical_loggers:
                    #print(logger.name)
                    logger.log('NN', samples=onp.array(network_samples))
                #print('Done')

                # test_samples = jax.random.choice(subkey, 4, shape=(10 ** 5, N), replace=True)
                # test_sample_weights = jnp.ones(len(test_samples)) / 4 ** N
                # target = target_mps.P(test_samples)
                # povm_loss, full_dist = trainingSampled.evaluate(optimizer.target, test_samples, test_sample_weights,
                #                                                 target)

            distribution_loggers['true_fid'].add_value('NN', povm_infid)
            distribution_loggers['dataset_fid'].add_value('NN', loss)

            slope = -1 #Todo: reinclude?
            #slope = distribution_loggers['true_fid'].relSlope('NN')

            if doPrint:
                src.info(f"{i}\t{metrics:.3e}      {grads:.3e}       {loss:.2e}     "
                         f"{slope:.2e}\t     {povm_infid:.2e}       ")

            if onp.isnan(metrics):  # training instability -> makes no sense to continue
                src.warn('\nHalt (Nan loss)')
                break

            if grads < 1e-6:
                src.warn('\nHalt (vanishing grads)')
                break

            if i > 1000 and i % 900 == 0 and not onp.isnan(slope) and (halt_slope < slope < 0):
                src.warn('\nHalt (slope)')
                break

            if haltLoss != -100 and 0 < povm_infid < haltLoss:
                src.warn('\nHalt (loss)')
                break
                
    print(povms.tensorDistTo1D(full_dist_t).tolist())

    duration = time.time() - start_time

    #if type(net) == AutoregressiveNNs.ARCNN:
    #    halfsystem_dist = AutoregressiveNNs.getSubsystemDist(net, optimizer.target, N//2, N)
    #    physical_loggers[3].add_value("Exact NN", povms.purity(povms.dist1DToTensor(halfsystem_dist, N//2)))

    network_samples = jax.device_get(ann.getSamples(net, optimizer.target, N,
                                                    params['NsEval'], key))
    #for logger in physical_loggers:
    #    logger.log('NN-MLE', samples_for_mle=onp.array(network_samples))

    res = {
        "numParams": ann.getNumParams(optimizer),
        #"finalSlope": m,
        #"fullDist": jax.device_get(full_dist).tolist(),
        "duration": duration,
        "numSteps": i,
        "final_nn_samples": network_samples
    }

    if return_nn_params:
        return res, optimizer.target
    else:
        return res
