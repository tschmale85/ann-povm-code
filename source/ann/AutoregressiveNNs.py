import jax
from flax import linen as nn
from jax import numpy as jnp, ops

import sys
sys.path.insert(0, '..')
sys.path.insert(0, '../..')

from source import quantum as qu, dms, povms, ann
import numpy as onp

paulis = jnp.array(qu.paulis)

class ARCNN(nn.Module):
    """A simple autoregressive CNN model."""
    features: tuple = None
    kernels: tuple = None
    actFns: tuple = None
    encoding: str = None

    requires_norm = False

    @nn.compact
    def __call__(self, x, return_conditionals=False):
        input_config = jnp.array(x)
        x = ann.encode(x, self.encoding)

        kernel_size = (self.kernels[0], ann.encoding_dimension[self.encoding], )

        #x.shape = (batch size, N, encoding dependant, 1)

        x = jnp.roll(x, 1, axis=1) # last spin is now in first site

        # Set N=0 values to zero, which now corresponds to last spin. the last spin should not affect any probabilities
        x = jax.ops.index_update(x, jax.ops.index[:, 0, :, :], 0)

        #pad along particle direction with zeros to ensure information only flows in correct direction
        p = self.kernels[0] - 1
        x = jnp.pad(x, ((0, 0), (p, 0), (0, 0), (0, 0)), 'constant', constant_values=0)

        x = nn.Conv(features=self.features[0], kernel_size=kernel_size,
                    kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in", distribution="normal"),
                    bias_init=jax.nn.initializers.zeros, use_bias=True, padding=((0, 0), (0, 0)))(x)
        x = self.actFns[0](x)

        x = jnp.mean(x, axis=2) #Average out input encoding dimension
        #x.shape = (batch size, N, features)

        if len(self.features) > 1:
            for f, k, fn in zip(self.features[1:], self.kernels[1:], self.actFns[1:]):
                pad = k - 1
                x = jnp.pad(x, ((0, 0), (pad, 0), (0, 0)), 'constant', constant_values=0)
                x = nn.Conv(features=f, kernel_size=(k,),
                            kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                             distribution="normal"),
                            bias_init=jax.nn.initializers.zeros, use_bias=True, padding=((0, 0), ))(x)
                x = fn(x)
        #x.shape = (batch size, N, features)
        x = nn.Dense(features=4)(x) #Ensure 4 outputs per site -> 4 local probabilities
        #x.shape = (batch size, N, 4)
        x = jax.nn.softmax(x) # -> 4 local probabilities

        if return_conditionals:
            return x #prob (not log!)

        correct_conditional_probs = takeProbs(x, input_config.astype(jnp.int32))
        # shape = (batch_size, N)
        correct_conditional_log_probs = jnp.log(correct_conditional_probs)
        # compute total probability P(a1, a2, ...) = P(a1)*P(a2|a1)*...
        log_probs = jnp.sum(correct_conditional_log_probs, axis=1)
        # shape = (batch_size, )
        return log_probs

def getSubsystemDist(net, net_params, subsystem_size, N):
    basis = ann.getBasis(subsystem_size) # get basis for subsystem
    basis = jnp.pad(basis, ((0, 0), (0, N-subsystem_size))) # pad with zero, so particle number is correct
    probs = net.apply({'params': net_params}, basis, return_conditionals=True) # compute conditionals
    probs = takeProbs(probs, basis.astype(jnp.int32))[:, :subsystem_size] # select correct conditionals
    return jnp.prod(probs, axis=1) # take products along particle dimension, to get probabilities


def sample(net, key, net_params, N, batch_size):
    init_samples = jnp.zeros(
        (batch_size, N))  # initial samples all zeros, as first spin outcome independent of input

    def sampleSingle(state, _):
        samples, key_, i = state

        key_, *subkeys = jax.random.split(key_, batch_size + 1)

        # compute conditional probabilities corresponding to current sample
        all_probs = net.apply({'params': net_params}, samples, return_conditionals=True)

        probs_at_i_th_site = all_probs[:, i, :]
        i_th_outcomes = multiDimChoice(jnp.array(subkeys),
                                       probs_at_i_th_site).squeeze()  # sample outcome of i'th spin
        samples = jax.ops.index_update(samples, jax.ops.index[:, i], i_th_outcomes)  # add sampled outcome to sample
        return (samples, key_, i + 1), _

    (res, *_), _ = jax.lax.scan(sampleSingle, (init_samples, key, 0), None, N)
    return res.astype(jnp.int32)

class ARCNN_2D_probs(nn.Module):
    """A simple autoregressive 2D CNN model."""
    features: tuple = None
    kernels: tuple = None
    actFns: tuple = None
    encoding: str = None
    L: int = None

    requires_norm = False

    @nn.compact
    def __call__(self, x):
        assert 1 == 0, "Not implemented"

        x = ann.encode(x, self.encoding)

        # x.shape = (batch size, N, encoding dependant, 1)

        x = jnp.roll(x, 1, axis=1)  # last spin is now in first site

        # Set N=0 values to zero, which now corresponds to last spin. the last spin should not affect any probabilities
        x = jax.ops.index_update(x, jax.ops.index[:, 0, :, :], 0)

        x = x.reshape(x.shape[0], self.L, self.L, x.shape[2], x.shape[3])

        # x.shape = (batch size, L, L, encoding dependant, 1)

        kernel_size = (self.kernels[0], self.kernels[0], ann.encoding_dimension[self.encoding], )

        #pad along particle direction with zeros to ensure information only flows in correct direction
        p = self.kernels[0] - 1
        x = jnp.pad(x, ((0, 0), (p, 0), (p, 0), (0, 0), (0, 0)), 'constant', constant_values=0)

        x = nn.Conv(features=self.features[0], kernel_size=kernel_size,
                    kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in", distribution="normal"),
                    bias_init=jax.nn.initializers.zeros, use_bias=True, padding=((0, 0), (0, 0), (0, 0)))(x)
        x = self.actFns[0](x)

        x = jnp.mean(x, axis=2) #Average out input encoding dimension
        #x.shape = (batch size, L, L, features)

        if len(self.features) > 1:
            for f, k, fn in zip(self.features[1:], self.kernels[1:], self.actFns[1:]):
                pad = k - 1
                x = jnp.pad(x, ((0, 0), (pad, 0), (pad, 0), (0, 0)), 'constant', constant_values=0)
                x = nn.Conv(features=f, kernel_size=(k,),
                            kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                             distribution="normal"),
                            bias_init=jax.nn.initializers.zeros, use_bias=True, padding=((0, 0), (0, 0)))(x)
                x = fn(x)
        #x.shape = (batch size, L, L, features)
        x = nn.Dense(features=4)(x) #Ensure 4 outputs per site -> 4 local probabilities
        #x.shape = (batch size, L, L, 4)
        x = jax.nn.softmax(x) # -> 4 local probabilities

        x = x.reshape(-1, self.L*self.L, 4)
        # x.shape = (batch size, N, 4)

        return x #prob (not log!)

@jax.vmap
def multiDimChoice(key, probs):
    return jax.random.choice(key, 4, (1,), p=probs) # choose one element out of {0, 1, 2, 3} with probabilities p

@jax.vmap
def takeProbs(probs, outcomes):
    return probs[jnp.arange(len(outcomes)), outcomes]


class ARNN(nn.Module):

    N = None

    requires_norm = False

    @nn.compact
    def __call__(self, x):
        x = jnp.array(x, dtype=int)
        #x.shape = (batch, N)
        P1 = SingleFactorNN()(x[:, :0]) #shape = (batch, 4)
        P2 = SingleFactorNN()(x[:, :1]) #shape = (batch, 4)
        P3 = SingleFactorNN()(x[:, :2]) #shape = (batch, 4)
        P4 = SingleFactorNN()(x[:, :3])  # shape = (batch, 4)
        #print(P1.shape, P2.shape, P3.shape, P4.shape)
        temp = jnp.arange(0, x.shape[0])
        return jnp.log(P1[temp, x[:, 0]] * P2[temp, x[:, 1]] * P3[temp, x[:, 2]] * P4[temp, x[:, 3]])

class SingleFactorNN(nn.Module):
    @nn.compact
    def __call__(self, x):
        #x = conditionals
        #print("Single: ", x.shape)
        x = nn.Dense(features=3, kernel_init=jax.nn.initializers.zeros)(x)
        return features_to_single_ptcl_povm(x)

def features_to_single_ptcl_povm(features):
    #features.shape = (batch, 3)
    rho = single_rho_param(features)
    #rho.shape = (batch, 2, 2)
    return rho_to_dist_vmapped(rho) #shape = (batch, 4)


rho_to_dist_vmapped = jax.vmap(povms.default_povm.get_single_ptcl_distribution)

@jax.vmap
def single_rho_param(R):
    #norm = jnp.linalg.norm(R)
    #R = jax.lax.cond(norm > 1, lambda x: R/norm, lambda x: R, None)
    return 0.5*(jnp.eye(2) + jnp.einsum('ijk, i', paulis[1:], R))


if __name__ == '__main__':
    features_ = onp.random.random(size=(100, 3))
    dists = features_to_single_ptcl_povm(features_)

    for dist in dists:
        rho_ = povms.default_povm.linRecon(dist)
        print(dms.isHermitean(rho_), onp.trace(rho_), dms.isPositive(rho_), jnp.min(jnp.linalg.eigvalsh(rho_)))
    #    r = onp.random.random(3)
    #    rho = single_rho_param(r)
    #    print(dms.isHermitean(rho), onp.trace(rho), dms.isPositive(rho), jnp.min(jnp.linalg.eigvalsh(rho)))
