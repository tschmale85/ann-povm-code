import jax
from flax import linen as nn
from jax import numpy as jnp
from source import ann


class DNN(nn.Module):
    """A simple DNN model."""

    features: tuple = None
    kernels: tuple = None
    actFns: tuple = None
    encoding: str = None

    requires_norm = True

    @nn.compact
    def __call__(self, x):
        x = ann.encode(x, self.encoding)

        # reshape physical and encoding dimension into 1 dimension, so dense layer acts on both
        x = x.reshape(x.shape[0], -1)
        x = nn.Dense(features=self.features[0],
                     kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in", distribution="normal"),
                     bias_init=jax.nn.initializers.zeros)(x)
        x = self.actFns[0](x)
        # x.shape = (batch, physical/feature)

        if len(self.features) > 1:
            for f, fn in zip(self.features[1:], self.actFns[1:]):
                x = nn.Dense(features=f, kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                                          distribution="normal"),
                             bias_init=jax.nn.initializers.zeros)(x)
                x = fn(x)

        x = nn.Dense(features=1)(x).squeeze()
        # x.shape = (batch, )

        return x #log_prob

class CNN(nn.Module):
    """A simple CNN model."""

    features: tuple = None
    kernels: tuple = None
    actFns: tuple = None
    encoding: str = None

    requires_norm = True

    @nn.compact
    def __call__(self, x):
        # shape = (batch, N)
        x = ann.encode(x, self.encoding)
        # shape = (batch, N, encoding, 1)

        kernel_size = (self.kernels[0], ann.encoding_dimension[self.encoding], )

        x = nn.Conv(features=self.features[0], kernel_size=kernel_size,
                    kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in", distribution="normal"),
                    bias_init=jax.nn.initializers.zeros)(x)
        x = self.actFns[0](x)
        # shape = (batch, N, encoding, features)

        x = jnp.mean(x, axis=2)

        #shape = (batch, N, features)

        if len(self.features) > 1:
            for f, k, fn in zip(self.features[1:], self.kernels[1:], self.actFns[1:]):
                x = nn.Conv(features=f, kernel_size=(k,),
                            kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                             distribution="normal"),
                            bias_init=jax.nn.initializers.zeros)(x)
                x = fn(x)

        x = x.reshape((x.shape[0], -1))  # flatten
        x = nn.Dense(features=1)(x)

        return x #log_prob

class CCNN(nn.Module):
    """A simple CCNN model."""

    features: tuple = None
    kernels: tuple = None
    actFns: tuple = None
    encoding: str = None

    requires_norm = True

    @nn.compact
    def __call__(self, x):
        x = ann.encode(x, self.encoding)

        kernel_size = (self.kernels[0], ann.encoding_dimension[self.encoding], )

        #x.shape = (batch size, N, encoding dependant, 1)

        #pad along particle direction with periodic boundary condition to ensure translation invariance
        p = self.kernels[0] - 1
        x = jnp.pad(x, ((0, 0), (p, 0), (0, 0), (0, 0)), 'wrap')

        x = nn.Conv(features=self.features[0], kernel_size=kernel_size,
                    kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in", distribution="normal"),
                    bias_init=jax.nn.initializers.zeros, use_bias=True, padding=((0, 0), (0, 0)))(x)
        x = self.actFns[0](x)

        x = jnp.mean(x, axis=2) #Average out input encoding dimension
        #x.shape = (batch size, N, features)

        if len(self.features) > 1:
            for f, k, fn in zip(self.features[1:], self.kernels[1:], self.actFns[1:]):
                pad = k - 1
                x = jnp.pad(x, ((0, 0), (pad, 0), (0, 0)), 'wrap')
                x = nn.Conv(features=f, kernel_size=(k,),
                            kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                             distribution="normal"),
                            bias_init=jax.nn.initializers.zeros, use_bias=True, padding=((0, 0), ))(x)
                x = fn(x)
        x = jnp.sum(x, axis=1) # sum over spin chain -> ensure translation invariance
        #x.shape = (batch size, features)
        x = nn.Dense(features=1)(x)
        #x.shape = (batch size)

        return x #log_prob

class CCNN_2D(nn.Module):
    """A simple CCNN model."""

    features: tuple = None
    kernels: tuple = None
    actFns: tuple = None
    encoding: str = None
    L: int = None

    requires_norm = True

    @nn.compact
    def __call__(self, x):
        x = ann.encode(x, self.encoding)

        x = x.reshape(x.shape[0], self.L, self.L, x.shape[2], x.shape[3])

        kernel_size = (self.kernels[0], self.kernels[0], ann.encoding_dimension[self.encoding],)

        # x.shape = (batch size, L, L, encoding dependant, 1)

        # pad along particle direction with periodic boundary condition to ensure translation invariance
        p = self.kernels[0]-1
        x = jnp.pad(x, ((0, 0), (p, 0), (p, 0), (0, 0), (0, 0)), 'wrap')
        x = nn.Conv(features=self.features[0], kernel_size=kernel_size,
                    kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                     distribution="normal"),
                    bias_init=jax.nn.initializers.zeros, use_bias=True, padding=((0, 0), (0, 0), (0, 0)))(x)

        x = self.actFns[0](x)

        x = jnp.mean(x, axis=3)  # Average out input encoding dimension
        # x.shape = (batch size, L, L, features)

        if len(self.features) > 1:
            for f, k, fn in zip(self.features[1:], self.kernels[1:], self.actFns[1:]):
                pad = k-1
                x = jnp.pad(x, ((0, 0), (pad, 0), (pad, 0), (0, 0)), 'wrap')
                x = nn.Conv(features=f, kernel_size=(k, k),
                            kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                             distribution="normal"),
                            bias_init=jax.nn.initializers.zeros, use_bias=True, padding=((0, 0), (0, 0)))(x)
                x = fn(x)
        x = jnp.einsum('ijkl -> il', x)  # sum over spins -> ensure translation invariance
        # x.shape = (batch size, features)
        x = nn.Dense(features=1)(x)
        # x.shape = (batch size)

        return x #log_prob

class CNN_2D(nn.Module):
    """A simple CNN model."""

    features: tuple = None
    kernels: tuple = None
    actFns: tuple = None
    encoding: str = None
    L: int = None

    requires_norm = True

    @nn.compact
    def __call__(self, x):
        x = ann.encode(x, self.encoding)

        x = x.reshape(x.shape[0], self.L, self.L, x.shape[2], x.shape[3])

        kernel_size = (self.kernels[0], self.kernels[0], ann.encoding_dimension[self.encoding],)

        # x.shape = (batch size, L, L, encoding dependant, 1)

        x = nn.Conv(features=self.features[0], kernel_size=kernel_size,
                    kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                     distribution="normal"),
                    bias_init=jax.nn.initializers.zeros, use_bias=True)(x)

        x = self.actFns[0](x)

        x = jnp.mean(x, axis=3)  # Average out input encoding dimension
        # x.shape = (batch size, L, L, features)

        if len(self.features) > 1:
            for f, k, fn in zip(self.features[1:], self.kernels[1:], self.actFns[1:]):
                x = nn.Conv(features=f, kernel_size=(k, k),
                            kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                             distribution="normal"),
                            bias_init=jax.nn.initializers.zeros, use_bias=True)(x)
                x = fn(x)
        x = x.reshape(x.shape[0], -1)
        # x.shape = (batch size, features*L*L)

        #x = jnp.einsum('ijkl -> il', x)
        x = nn.Dense(features=1)(x)
        # x.shape = (batch size)

        return x #log_prob


class CCNNExplicit(nn.Module):
    """
        Circular a.k.a. translation invariant CNN. Implemented by explicitly summing over all lattice translations.
        Way slower than CCNN..
    """

    features: tuple = None
    kernels: tuple = None
    actFns: tuple = None
    encoding: str = None

    requires_norm = True

    #@nn.compact
    def setup(self):

        kernel_size = (self.kernels[0], ann.encoding_dimension[self.encoding], )

        layers_temp = list()

        layers_temp.append(nn.Conv(features=self.features[0], kernel_size=kernel_size,
                                   kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                                    distribution="normal"),
                                   bias_init=jax.nn.initializers.zeros, use_bias=True))
        if len(self.features) > 1:
            for f, k in zip(self.features[1:], self.kernels[1:]):
                layers_temp.append(nn.Conv(features=f, kernel_size=(k,),
                                           kernel_init=jax.nn.initializers.variance_scaling(scale=1.0, mode="fan_in",
                                                                                            distribution="normal"),
                                           bias_init=jax.nn.initializers.zeros, use_bias=True))
        self.layers = layers_temp
        self.finalLayer = nn.Dense(features=1)

    @nn.compact
    def __call__(self, x):
        N = x.shape[1]
        res = 0
        for i in range(N):
            x_ = jnp.roll(x, i, axis=1)
            res += self.__call2__(x_)
        return res

    def __call2__(self, x):
        x = ann.encode(x, encoding=self.encoding)
        x = self.layers[0](x)
        x = self.actFns[0](x)

        x = jnp.mean(x, axis=2)

        if len(self.features) > 1:
            for layer, fn in zip(self.layers[1:], self.actFns[1:]):
                x = layer(x)
                x = fn(x)
        x = x.reshape((x.shape[0], -1))  # flatten
        x = self.finalLayer(x)

        return x #log_prob
