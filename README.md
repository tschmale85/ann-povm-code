# ann-povm

This package allows for training [Neural Quantum States](https://arxiv.org/abs/1606.02318) in which an artificial neural network learns to represent a mixed quantum state in terms of its [POVM](https://en.wikipedia.org/wiki/POVM) probability distribution.
Included are some basic functions to generate density matrices of common states as well as utilities for converting between density matrix representations and POVM representations of states ([Details](https://arxiv.org/pdf/1912.11052.pdf)).

## Installation

This package is based on the [jax](https://github.com/google/jax) library which currently only works on Linux/MacOS. However Windows users can install [WSL2](https://docs.microsoft.com/de-de/windows/wsl/install-win10) and use this package just fine.


```bash
pip install --upgrade pip
pip install numpy
pip install -r requirements.txt
```

## Usage

`main.py` is the starting point for training neural networks. <br>
`genGroundstates.py` is the starting point for computing exact groundstates (as test-inputs) <br>
`gen_samples.py` is the starting point for simulating measurements on these states by generating samples from their respective POVM distributions.

See all options with

```bash
python main.py --help
```