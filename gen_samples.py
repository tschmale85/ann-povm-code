import argparse
from pathlib import Path
import os
import pickle

# only use one thread per MC chain
#os.environ["XLA_FLAGS"] = ("--xla_cpu_multi_thread_eigen=false "
#                           "intra_op_parallelism_threads=2")
import jax
import numpy as np
from source.quantum import graph_states, ising, povms
from source import sampling
import source as src

parser = argparse.ArgumentParser()
parser.add_argument('-N', help='Number of particles', type=int)
parser.add_argument('-Ns', help='Number of samples', type=int)
parser.add_argument('-dir', help='Output directory', type=str)
parser.add_argument('-o', help='Output file name', type=str, default=None)
parser.add_argument('-c', '--chains', help='Number of concurrent Markov chains', type=int, default=16)
parser.add_argument('-b', '--burn_in', help='Number of initial samples to discard for every Markov Chain',
                    type=int, default=10000)

subparsers = parser.add_subparsers(help='Type of state to train', dest='type')

ising_parser = subparsers.add_parser('ising')
ising_parser.add_argument("-J", help="Ising coupling constant", type=float, default=1.0)
ising_parser.add_argument("-B", help="Ising external field", type=float, default=1.0)
ising_parser.add_argument("--dim", help="Dimension (1/2)", type=int, default=1)
ising_parser.add_argument("--alpha", help="Decay of interaction", type=float, default=None)

graph_parser = subparsers.add_parser('graph')

cnot_parser = subparsers.add_parser('cnot')

ensemble_parser = subparsers.add_parser('ensemble')
ensemble_parser.add_argument("-f", help="Ensemble file", type=str, required=True)

args = parser.parse_args()

Path(args.dir).mkdir(parents=True, exist_ok=True)

if args.o is not None:
    file_name = args.o
else:
    file_name = None

samples = None

if args.type == 'ising':
    src.info(f"Trying to load state for N={args.N}, J={args.J}, B={args.B}, dim={args.dim}, alpha={args.alpha}")
    state = ising.loadState(args.J, args.B, args.N, dim=args.dim, alpha=args.alpha)

    #P = povms.ppp(state, do_jit=False) # not picklable :(
    samples = None
    try:
        samples = sampling.multiChainMC(args.Ns, state, args.N, args.chains, burn_in=args.burn_in, local=True)
    except Exception as e:
        src.warn("gen_samples.py: " + str(e))

    if args.o is None:
        file_name = str({'N': args.N, 'J': args.J, 'B': args.B, 'dim': args.dim,
                         'alpha': args.alpha}).replace(':', '=')

elif args.type == 'graph':
    if args.o is None:
        file_name = os.path.join(args.dir, f"data{args.N}")

    samples = np.array(graph_states.ClusterMPS(args.N).generateSamples(args.Ns))

elif args.type == 'cnot':
    from source.quantum import cnot_states

    if args.o is None:
        file_name = f"cnot{args.N}"
    state = cnot_states.get_state(args.N)

    samples = sampling.multiChainMC(args.Ns, state, args.N, args.chains, burn_in=args.burn_in, local=False)

elif args.type == 'ensemble':
    file = args.f

    if args.o is None:
        file_name = "ensemble"

    with open(file, "rb") as f:
        ensemble = pickle.load(f)
    ensemble = np.array([state.full() for state in ensemble])

    #Ns_per_state = np.ceil(args.Ns/len(ensemble))

    key = jax.random.PRNGKey(np.random.randint(0, 10 ** 8, size=1)[0])

    logP = jax.vmap(povms.default_povm.logP)

    def logP_on_ensemble(a):
        return logP(a, ensemble)

    samples = sampling.getLocalSamples(key, logP_on_ensemble, args.N, num_samples=args.Ns, burn_in=args.burn_in,
                                       num_chains=len(ensemble))

    #ensemble_samples = []
    #for state in ensemble:
    #    state = state.full().squeeze()
    #    ensemble_samples.append(sampling.multiChainMC(Ns_per_state, state, args.N, args.chains, burn_in=args.burn_in))

    #samples = np.vstack(ensemble_samples).astype(np.int32)

else:
    src.warn("Invalid Quantum State!")
    exit(1)

if samples is None or len(samples) == 0:
    src.warn("Error, no samples generated!")
    exit(1)

file_name += "_" + str(np.random.uniform())
with open(os.path.join(args.dir, file_name + ".txt"), 'w') as file:
    for sample in samples:
        file.write(str(sample)[1:-1].replace('\n', '').replace(' ', '') + "\n")
#np.save(file_name, samples)
